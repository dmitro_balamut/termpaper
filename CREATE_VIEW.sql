CREATE VIEW AdminUser
AS
SELECt Users.Id, Users.Surname, Users.Firstname, Users.Lastname, Roles.Name FROM Users inner join Roles ON Users.RoleID = Roles.Id
WHERE Roles.Name != '��������' and Roles.Name != '������������'


CREATE VIEW DisciplineName
AS
SELECT        dbo.Discipline.Name, dbo.Users.Surname, dbo.Users.Firstname, dbo.Users.Lastname, dbo.Commission.[Name] AS 'Commission'
FROM            dbo.Discipline INNER JOIN
                         dbo.UserDiscipline ON dbo.Discipline.Id = dbo.UserDiscipline.DisciplineID INNER JOIN
                         dbo.Users ON dbo.UserDiscipline.UserID = dbo.Users.Id inner join Commission ON Discipline.CommissionID = Commission.Id

CREATE VIEW GroupName
AS
SELECT        dbo.NameGroup.Name, dbo.Users.Surname, dbo.Course.Name AS Expr1, dbo.Formofstudy.Name AS Expr2
FROM            dbo.Course INNER JOIN
                         dbo.NameGroup ON dbo.Course.Id = dbo.NameGroup.CourseID INNER JOIN
                         dbo.Users ON dbo.NameGroup.UserID = dbo.Users.Id INNER JOIN
                         dbo.Formofstudy ON dbo.NameGroup.FormofstudyID = dbo.Formofstudy.Id

CREATE VIEW PracticWork
AS
SELECT        dbo.PracticalWork.Name, dbo.[File].Way, dbo.PracticalWork.DateFirst, dbo.PracticalWork.DateLast, dbo.Discipline.Name AS Expr1, dbo.Users.Surname, dbo.Users.Firstname, dbo.Users.Lastname
FROM            dbo.PracticalWork INNER JOIN
                         dbo.[File] ON dbo.PracticalWork.Id = dbo.[File].PracticalWorkID INNER JOIN
                         dbo.Discipline ON dbo.PracticalWork.DisciplineID = dbo.Discipline.Id INNER JOIN
                         dbo.UserDiscipline ON dbo.Discipline.Id = dbo.UserDiscipline.DisciplineID INNER JOIN
                         dbo.Users ON dbo.UserDiscipline.UserID = dbo.Users.Id


CREATE VIEW Student_Group
AS
SELECT        dbo.NameGroup.Name, dbo.Users.Id, dbo.Users.Surname
FROM            dbo.NameGroup INNER JOIN
                         dbo.UserGroup ON dbo.NameGroup.Id = dbo.UserGroup.NameGroupID INNER JOIN
                         dbo.Users ON dbo.UserGroup.UserID = dbo.Users.Id


CREATE VIEW UserComm
AS
SELECT        dbo.Commission.Name, dbo.Users.Surname, dbo.Users.Firstname, dbo.Users.Lastname
FROM            dbo.Commission INNER JOIN
                         dbo.CommissionUser ON dbo.Commission.Id = dbo.CommissionUser.CommissionID INNER JOIN
                         dbo.Users ON dbo.CommissionUser.UserID = dbo.Users.Id

CREATE VIEW Practical
AS
SELECT 

Users.Surname, Users.Firstname, Users.Lastname, PracticalWork.[Name], [File].Way, Discipline.Name 

FROM PracticalWork inner join [File] ON PracticalWork.Id = [File].PracticalWorkID inner join Discipline 
ON PracticalWork.DisciplineID = Discipline.Id inner join UserDiscipline ON Discipline.Id = UserDiscipline.DisciplineID
inner join Users ON UserDiscipline.UserID = Users.Id