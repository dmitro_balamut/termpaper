declare @path NVARCHAR(MAX)
set @path = 'D:\PortalDB.bak'
backup database PortalDB
to disk = @path with init,
name = 'Portal Full Backup',
description = 'Portal Full Backup'